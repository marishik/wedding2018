$(function () {
    var menu = $('.navbar-menu');
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 140) {
            if (!menu.hasClass('open')) {
                menu.addClass('open');
            }
        } else {
            if (menu.hasClass('open')) {
                menu.removeClass('open');
                menu.find('.show').removeClass('show');
            }
        }
    });
});

$.fn.share = function (width, height, fn) {
    var og = $('meta[property]');
    this.on('click', function (e) {
        var urlParams = {},
            winParams = 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes'
                + ',width= ' + width.toFixed()
                + ',height=' + height.toFixed()
                + ',left=' + ((screen.availWidth - width) / 2).toFixed()
                + ',top=' + ((screen.availHeight - height) / 2).toFixed();

        og.each(function () {
            var p = this.getAttribute('property').replace(/\w+:/, '');
            urlParams[p] = this.getAttribute('content');
        });

        if ('function' === typeof fn) {
            urlParams = fn.call(this, urlParams);
        }

        this.href = $(this).data('href').replace(/\{(\w+)\}/g, function () {
            var p = arguments[1];
            return 'undefined' === typeof urlParams[p] ? '' : encodeURIComponent(urlParams[p]);
        });

        open(this.href, '', winParams) && e.preventDefault();
    });
};

$(function () {
    $('[data-share]').share(600, 400, function (data) {
        trackShare($(this).data('share'));
        return data;
    });
});

// Animation
(function () {
    var vector = {
        x: 0,
        y: 0,

        create: function (x, y) {
            var item = Object.create(this);
            item.x = x;
            item.y = y;
            return item;
        },

        getAngle: function () {
            return Math.atan2(this.x, this.y);
        },

        setAngle: function (angle) {
            var length = this.getLength();
            this.x = Math.cos(angle) * length;
            this.y = Math.sin(angle) * length;
        },

        getLength: function () {
            return Math.sqrt(this.x * this.x + this.y * this.y);
        },

        setLength: function (length) {
            var angle = this.getAngle();
            this.x = Math.cos(angle) * length;
            this.y = Math.sin(angle) * length;
        },

        add: function (other) {
            return vector.create(this.x + other.x, this.y + other.y);
        },

        addTo: function (other) {
            this.x += other.x;
            this.y += other.y;
        }
    };

    var heart = {
        position: null,
        velocity: null,
        phase: 0,
        color: "IndianRed",

        create: function (x, y, speed, direction) {
            var item = Object.create(this);
            item.position = vector.create(x, y);
            item.velocity = vector.create(0, 0);
            item.velocity.setLength(speed);
            item.velocity.setAngle(direction);
            item.phase = Math.random() * Math.PI * 2;
            return item;
        },

        update: function () {
            this.position.addTo(this.velocity);
            this.phase += Math.PI / 60;
        },

        draw: function (context) {
            context.save();

            context.fillStyle = this.color;
            var radius = 3 * (2 + Math.sin(this.phase) / 2);

            context.translate(this.position.x, this.position.y);
            var angle = this.velocity.getAngle();
            if (Math.cos(angle) < 0) {
                angle = Math.PI - angle;
            }

            context.rotate(angle);

            context.beginPath();
            context.arc(-radius, -radius, radius, 0, 5 * Math.PI / 6, true);
            context.lineTo(0, 2 * radius);
            context.arc(radius, -radius, radius, Math.PI / 6, Math.PI, true);
            context.lineTo(0, 2 * radius);
            context.fill();

            context.restore();
        }
    };

    var scene = {
        width: 0,
        height: 0,
        particles: null,
        size: 10,
        gravity: null,

        create: function (width, height, gravity) {
            var item = Object.create(this);
            item.resize(width, height);
            item.particles = [];
            item.gravity = vector.create(0, gravity);
            return item;
        },

        addHearts: function(count) {
            var x = 0, y = -0.1 * this.height,
                angle = Math.atan2(0.5 * this.width, 0.9 * this.height);
            for (var i = 0; i < count; ++i) {
                this.particles.push(heart.create(x, y, Math.random() * 4 + 1, (1 - 2 * Math.random()) * angle + 1.5 * Math.PI));
            }
            window.console && console.log('Scene: %d hearts added', count);
        },

        resize: function (width, height) {
            this.width = width;
            this.height = height;
            window.console && console.log('Scene: resized to %dx%d', width, height);
        },

        update: function () {
            for (var i = this.particles.length - 1; i >= 0; --i) {
                var particle = this.particles[i];
                particle.velocity.addTo(this.gravity);
                particle.update();
                if (particle.position.y > this.size) {
                    this.particles.splice(i, 1);
                }
            }
        },

        draw: function (context) {
            context.clearRect(0, 0, this.width, this.height);
            context.save();
            context.translate(this.width / 2, this.height);
            for (var i in this.particles) {
                this.particles[i].draw(context);
            }
            context.restore();
        }
    };

    $(function () {
        var canvas = $('#canvas')[0],
            context = canvas.getContext('2d'),
            heartsScene = scene.create(canvas.width, canvas.height, 0.005);

        $(window).on('resize', function () {
            canvas.width = $(window).width();
            canvas.height = $(window).height();
            heartsScene.resize(canvas.width, canvas.height);
        }).triggerHandler('resize');

        heartsScene.addHearts(heartsScene.width / Math.log(heartsScene.width) / Math.E);

        function update(time) {
            ++update.frames;
            update.time = time;

            heartsScene.update();
            heartsScene.draw(context);
            if (heartsScene.particles.length > 0) {
                requestAnimationFrame(update);
            } else {
                window.console && console.log('Scene: animation finished');
            }
        }
        update.frames = 0;
        update.time = 0;

        setTimeout(function () {
            requestAnimationFrame(update);
        }, 1000);
    });
})();
