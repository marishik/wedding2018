const gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    pug = require('gulp-pug'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps');

function html() {
    return gulp.src('pug/*.pug')
        .pipe(pug({
            pretty: '    '
        }))
        .pipe(gulp.dest('public/'));
}

function css() {
    return gulp.src('scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['node_modules/bootstrap/scss/'],
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/css/'));
}

exports.html = html;
exports.css = css;
exports.default = gulp.parallel([html, css]);
